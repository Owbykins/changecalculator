﻿using ChangeCalculator.Services.Models;

namespace ChangeCalculator.Services
{
    public interface IChangeCalculator
    {
        Change CalculateChange(decimal itemCost, decimal amountGiven);
    }
}