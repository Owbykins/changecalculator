﻿using ChangeCalculator.Services.Models;
using System;

namespace ChangeCalculator.Services
{
    public class ChangeCalculator : IChangeCalculator
    {
        private const int FiftyNoteValue = 5000;
        private const int TwentyNoteValue = 2000;
        private const int TenNoteValue = 1000;
        private const int FiveNoteValue = 500;
        private const int TwoPoundValue = 200;
        private const int OnePoundValue = 100;
        private const int FiftyPenceValue = 50;
        private const int TwentyPenceValue = 20;
        private const int TenPenceValue = 10;
        private const int FivePenceValue = 5;
        private const int TwoPenceValue = 2;
        private const int OnePenceValue = 1;

        public Change CalculateChange(decimal itemCost, decimal amountGiven)
        {
            if (amountGiven < itemCost)
                throw new Exception("itemCost is greater than amountGiven");

            decimal changeLeft = amountGiven - itemCost;

            //Convert change value to integer
            int convertedValue = Convert.ToInt32(changeLeft * 100);

            //£50 notes
            int fiftyNotes = convertedValue / FiftyNoteValue;
            int reducedChangeTotal = convertedValue - (fiftyNotes * FiftyNoteValue);

            //£20 notes
            int twentyNotes = reducedChangeTotal / TwentyNoteValue;
            reducedChangeTotal = reducedChangeTotal - (twentyNotes * TwentyNoteValue);

            //£10 notes
            int tenNotes = reducedChangeTotal / TenNoteValue;
            reducedChangeTotal = reducedChangeTotal - (tenNotes * TenNoteValue);

            //£5 notes
            int fiveNotes = reducedChangeTotal / FiveNoteValue;
            reducedChangeTotal -= (fiveNotes * FiveNoteValue);

            //£2 coin
            int twoCoins = reducedChangeTotal / TwoPoundValue;
            reducedChangeTotal -= (twoCoins * TwoPoundValue);

            //£1 coin
            int oneCoins = reducedChangeTotal / OnePoundValue;
            reducedChangeTotal -= (oneCoins * OnePoundValue);

            //50p coin
            int fiftyPences = reducedChangeTotal / FiftyPenceValue;
            reducedChangeTotal -= (fiftyPences * FiftyPenceValue);

            //20p coin
            int twentyPences = reducedChangeTotal / TwentyPenceValue;
            reducedChangeTotal -= (twentyPences * TwentyPenceValue);

            //10p coin
            int tenPences = reducedChangeTotal / TenPenceValue;
            reducedChangeTotal -= (tenPences * TenPenceValue);

            //5p coin
            int fivePences = reducedChangeTotal / FivePenceValue;
            reducedChangeTotal -= (fivePences * FivePenceValue);

            //2p coin
            int twoPences = reducedChangeTotal / TwoPenceValue;
            reducedChangeTotal -= (twoPences * TwoPenceValue);

            //1p coin
            int onePences = reducedChangeTotal / OnePenceValue;
            reducedChangeTotal -= (onePences * OnePenceValue);

            if (reducedChangeTotal > 0 || reducedChangeTotal < 0)
                throw new Exception("change total is wrong");

            return new Change
            {
                FiftyPoundNotes = fiftyNotes,
                TwentyPoundNotes = twentyNotes,
                TenPoundNotes = tenNotes,
                FivePoundNotes = fiveNotes,
                TwoPoundCoins = twoCoins,
                OnePoundCoins = oneCoins,
                FiftyPenceCoins = fiftyPences,
                TwentyPenceCoins = twentyPences,
                TenPenceCoins = tenPences,
                FivePenceCoins = fivePences,
                TwoPenceCoins = twoPences,
                OnePenceCoins = onePences
            };
        }
    }
}
