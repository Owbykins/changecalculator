﻿namespace ChangeCalculator.Services.Models
{
    public class Change
    {

        public int FiftyPoundNotes { get; set; }
        public int TwentyPoundNotes { get; set; }
        public int TenPoundNotes { get; set; }
        public int FivePoundNotes { get; set; }
        public int TwoPoundCoins { get; set; }
        public int OnePoundCoins { get; set; }
        public int FiftyPenceCoins { get; set; }
        public int TwentyPenceCoins { get; set; }
        public int TenPenceCoins { get; set; }
        public int FivePenceCoins { get; set; }
        public int TwoPenceCoins { get; set; }
        public int OnePenceCoins { get; set; }
    }
}
