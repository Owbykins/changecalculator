using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;

namespace ChangeCalculator.Tests
{
    public class ChangeCalculatorTests
    {
        private Services.ChangeCalculator _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new Services.ChangeCalculator();
        }

        [Test]
        public void GivenAmounts_WhenChangeCalculated_ThenCorrectAmountReturned()
        {
            //Arrange
            var itemPrice = 11.12m;
            var amountGiven = 100.00m;

            //This will equal �88.88, which works out to 1 of each note/coin

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            using (new AssertionScope())
            {
                result.FiftyPoundNotes.Should().Be(1);
                result.TwentyPoundNotes.Should().Be(1);
                result.TenPoundNotes.Should().Be(1);
                result.FivePoundNotes.Should().Be(1);
                result.TwoPoundCoins.Should().Be(1);
                result.OnePoundCoins.Should().Be(1);
                result.FiftyPenceCoins.Should().Be(1);
                result.TwentyPenceCoins.Should().Be(1);
                result.TenPenceCoins.Should().Be(1);
                result.FivePenceCoins.Should().Be(1);
                result.TwoPenceCoins.Should().Be(1);
                result.OnePenceCoins.Should().Be(1);
            }
        }

        [TestCase(40, 0)]
        [TestCase(50, 1)]
        [TestCase(1000, 20)]
        public void GivenItemCostIsFiftyPounds_WhenChangeCalculated_ThenCorrectAmountOfFiftyNotesReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;
            
            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.FiftyPoundNotes.Should().Be(expectedAmount);
        }

        [TestCase(21, 1)]
        [TestCase(40, 2)]
        [TestCase(15, 0)]
        public void GivenItemCostIsOnePound_WhenChangeCalculated_ThenCorrectAmountOfTwentyNotesReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.TwentyPoundNotes.Should().Be(expectedAmount);
        }

        [TestCase(10, 1)]
        [TestCase(15, 1)]
        [TestCase(19, 1)]
        public void GivenItemCostIsOnePound_WhenChangeCalculated_ThenCorrectAmountOfTenNotesReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.TenPoundNotes.Should().Be(expectedAmount);
        }

        [TestCase(5, 1)]
        [TestCase(8, 1)]
        [TestCase(3, 0)]
        public void GivenItemCostIsOnePound_WhenChangeCalculated_ThenCorrectAmountOfFiveNotesReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.FivePoundNotes.Should().Be(expectedAmount);
        }

        [TestCase(2, 1)]
        [TestCase(4, 2)]
        [TestCase(1, 0)]
        public void GivenItemCostIsOnePound_WhenChangeCalculated_ThenCorrectAmountOfTwoPoundCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.TwoPoundCoins.Should().Be(expectedAmount);
        }

        [TestCase(1, 1)]
        [TestCase(0.5, 0)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfOnePoundCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.OnePoundCoins.Should().Be(expectedAmount);
        }

        [TestCase(0.6, 1)]
        [TestCase(0.5, 1)]
        [TestCase(0.4, 0)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfFiftyPenceCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.FiftyPenceCoins.Should().Be(expectedAmount);
        }

        [TestCase(0.1, 0)]
        [TestCase(0.2, 1)]
        [TestCase(0.3, 1)]
        [TestCase(0.4, 2)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfTwentyPenceCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.TwentyPenceCoins.Should().Be(expectedAmount);
        }

        [TestCase(0.05, 0)]
        [TestCase(0.1, 1)]
        [TestCase(0.2, 0)]
        [TestCase(0.3, 1)]
        [TestCase(0.4, 0)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfTenPenceCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.TenPenceCoins.Should().Be(expectedAmount);            
        }

        [TestCase(0.05, 1)]
        [TestCase(0.1, 0)]
        [TestCase(0.15, 1)]
        [TestCase(52.25, 1)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfFivePenceCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.FivePenceCoins.Should().Be(expectedAmount);
        }

        [TestCase(0.01, 0)]
        [TestCase(0.02, 1)]
        [TestCase(0.03, 1)]
        [TestCase(0.04, 2)]
        [TestCase(400.04, 2)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfTwoPenceCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.TwoPenceCoins.Should().Be(expectedAmount);
        }

        [TestCase(0.01, 1)]
        [TestCase(100.01, 1)]
        [TestCase(100.00, 0)]
        public void GivenItemCostIsZero_WhenChangeCalculated_ThenCorrectAmountOfOnePenceCoinsAreReturned(decimal amountGiven, int expectedAmount)
        {
            //Arrange
            var itemPrice = 0.00m;

            //Act
            var result = _sut.CalculateChange(amountGiven: amountGiven, itemCost: itemPrice);

            //Assert
            result.OnePenceCoins.Should().Be(expectedAmount);
        }
    }
}