﻿using ChangeCalculator.Models;
using ChangeCalculator.Services;
using ChangeCalculator.Services.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace ChangeCalculator.Controllers
{
    public class ChangeCalculatorController : Controller
    {
        private readonly ILogger<ChangeCalculatorController> _logger; //Was in for debugging
        private readonly IChangeCalculator _changeCalculator;

        public ChangeCalculatorController(ILogger<ChangeCalculatorController> logger,
                                          IChangeCalculator changeCalculator)
        {
            _logger = logger;
            _changeCalculator = changeCalculator;
        }

        [HttpPost]
        public Change Post([FromBody] CalcChangeRequest request)
        {
            var formattedRequest = ValidateAndFormatRequest(request);

            return _changeCalculator.CalculateChange(formattedRequest.Item1, formattedRequest.Item2);
        }

        private Tuple<decimal, decimal> ValidateAndFormatRequest(CalcChangeRequest request)
        {
            if (request == null)
                throw new System.Exception("Request cannot be empty");

            decimal itemCost;
            decimal.TryParse(request.ItemCost, out itemCost);
            decimal amountGiven;
            decimal.TryParse(request.AmountGiven, out amountGiven);

            return Tuple.Create(itemCost, amountGiven);
        }
    }
}
