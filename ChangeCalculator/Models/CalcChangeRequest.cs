﻿namespace ChangeCalculator.Models
{
    public class CalcChangeRequest
    {
        public string ItemCost { get; set; }
        public string AmountGiven { get; set; }
    }
}
