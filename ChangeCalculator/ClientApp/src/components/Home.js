import React, { Component } from 'react';
export class Home extends Component {
    static displayName = Home.name;

    constructor() {
        super();
        this.state = {
            itemCost: '0',
            amountGiven: '0',
            changeData: {
                fiftyPenceCoins: 0,
                fiftyPoundNotes: 0,
                fivePenceCoins: 0,
                fivePoundNotes: 0,
                onePenceCoins: 0,
                onePoundCoins: 0,
                tenPenceCoins: 0,
                tenPoundNotes: 0,
                twentyPenceCoins: 0,
                twentyPoundNotes: 0,
                twoPenceCoins: 0,
                twoPoundCoins: 0,
            }
        };
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit = (e) => {
        e.preventDefault();

        this.callController(this.state.itemCost, this.state.amountGiven);

        console.log(this.state.changeData);
    }

    async callController(itemCost, amountGiven) {
        const settings = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ itemCost: itemCost, amountGiven: amountGiven })
        };
        try {
            var response = await fetch(`/changecalculator/post`, settings)
            var data = await response.json();
            this.setState({ changeData: data });
        } catch (e) {
            return e;
        }
    }

    render() {
        const { itemCost, amountGiven, changeData } = this.state;
        return (
            <div>
                <p>Hello and welcome to my calculator! <br/> please enter the Item's cost and the amount given below and press submit. <br/> This will give you the amount of change required below. </p>

                <form onSubmit={this.onSubmit}>
                    <label>Item cost:</label>
                    <input type="number" name="itemCost" value={itemCost} step="any" onChange={this.onChange}></input>
                    <label>Amount given:</label>
                    <input type="number" name="amountGiven" value={amountGiven} step="any" onChange={this.onChange}></input>
                    <button type="submit">Submit</button>
                </form>

                <table>
                    <tr>
                        <th>Field Name</th>
                        <th>Value</th>
                    </tr>
                    <tr>
                        <td>Fifty pound notes (�50)</td>
                        <td>{changeData.fiftyPoundNotes}</td>
                    </tr>
                    <tr>
                        <td>Twenty pound notes (�20)</td>
                        <td>{changeData.twentyPoundNotes}</td>
                    </tr>
                    <tr>
                        <td>Ten pound notes (�10)</td>
                        <td>{changeData.tenPoundNotes}</td>
                    </tr>
                    <tr>
                        <td>Five pound notes (�5)</td>
                        <td>{changeData.fivePoundNotes}</td>
                    </tr>
                    <tr>
                        <td>Two pound coins (�2)</td>
                        <td>{changeData.twoPoundCoins}</td>
                    </tr>
                    <tr>
                        <td>One pound coins (�1)</td>
                        <td>{changeData.onePoundCoins}</td>
                    </tr>
                    <tr>
                        <td>Fifty Pence coins (50p)</td>
                        <td>{changeData.fiftyPenceCoins}</td>
                    </tr>
                    <tr>
                        <td>Twenty Pence coins (20p)</td>
                        <td>{changeData.twentyPenceCoins}</td>
                    </tr>
                    <tr>
                        <td>Ten Pence coins (10p)</td>
                        <td>{changeData.tenPenceCoins}</td>
                    </tr>
                    <tr>
                        <td>Five Pence coins (5p)</td>
                        <td>{changeData.fivePenceCoins}</td>
                    </tr>
                    <tr>
                        <td>Two Pence coins (2p)</td>
                        <td>{changeData.twoPenceCoins}</td>
                    </tr>
                    <tr>
                        <td>One Pence coins (1p)</td>
                        <td>{changeData.onePenceCoins}</td>
                    </tr>
                </table>
            </div>
        );
    }
}
